<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function() {
	Route::get('/', [
        'as' => 'admin', 'uses' => 'Admin@index','middleware' => 'auth' 
    ]);
    Route::get('/masjid', [
        'as' => 'masjid', 'uses' => 'Masjid@index','middleware' => 'auth' 
    ]);
    Route::get('/masjid/create', [
        'as' => 'add-masjid', 'uses' => 'Masjid@create','middleware' => 'auth' 
    ]);
    Route::post('/masjid', [
        'as' => 'save-masjid', 'uses' => 'Masjid@store','middleware' => 'auth' 
    ]);
    Route::get('/masjid/{id}/edit', [
        'as' => 'masjid.edit', 'uses' => 'Masjid@edit','middleware' => 'auth' 
    ]);
    Route::patch('/masjid/{id}', [
        'as' => 'update.masjid', 'uses' => 'Masjid@update','middleware' => 'auth' 
    ]);
    Route::get('/masjid/{id}', [
        'as' => 'masjid.show', 'uses' => 'Masjid@show','middleware' => 'auth' 
    ]);
    Route::delete('/masjid/{id}', [
        'as' => 'masjid.destroy', 'uses' => 'Masjid@destroy','middleware' => 'auth' 
    ]);
    Route::get('/jamaah', [
        'as' => 'masjid.jamaah', 'uses' => 'Masjid@jamaah','middleware' => 'auth' 
    ]);

    Route::resource('category', 'Category')->middleware('auth');;
    Route::resource('subcategory', 'SubCategory')->middleware('auth');;
    Route::resource('pembangunan', 'Pembangunan')->middleware('auth');;
    Route::resource('berita', 'Berita')->middleware('auth');;
    Route::resource('sejarah', 'Sejarah')->middleware('auth');;
    Route::resource('visimisi', 'VisiMisi')->middleware('auth');;
    Route::resource('dkm', 'Dkm')->middleware('auth');;
    Route::resource('page', 'Page')->middleware('auth');;
    Route::resource('team', 'Team')->middleware('auth');;
    Route::resource('donasimasjid', 'DonasiMasjid')->middleware('auth');;
    Route::resource('donasiyukamal', 'DonasiYukamal')->middleware('auth');;
    Route::resource('pengguna', 'Pengguna')->middleware('auth');;
    Route::resource('keuangan', 'MasjidFinance')->middleware('auth');;
    Route::resource('pembangunan_masjid', 'PembangunanMasjid')->middleware('auth');;
    Route::resource('event', 'Event')->middleware('auth');;
    Route::resource('galeri', 'Galeri')->middleware('auth');;
    Route::resource('profilpengguna', 'Profil')->middleware('auth');;
    Route::get('/profilpengguna/{id}/changepassword', [
        'as' => 'profilpengguna.password', 'uses' => 'Profil@password','middleware' => 'auth' 
    ]);
    Route::patch('/profilpengguna/{id}/updatepassword', [
        'as' => 'profilpengguna.updatepassword', 'uses' => 'Profil@update_password','middleware' => 'auth' 
    ]);

    Route::resource('masjid_account', 'MasjidAccount')->middleware('auth');;
    Route::resource('useryukamal', 'UserYukamal')->middleware('auth');;
    Route::resource('provinsi', 'Provinsi')->middleware('auth');;
    Route::resource('kota', 'Kota')->middleware('auth');;
    Route::resource('kecamatan', 'Kecamatan')->middleware('auth');;
    Route::resource('kelurahan', 'Kelurahan')->middleware('auth');;
});

Route::group(['prefix' => '/'], function() {
    Route::get('/', [
        'as' => 'home', 'uses' => 'HomeController@index' 
    ]);
    Route::get('/donasi', [
        'as' => 'donasi', 'uses' => 'HomeController@donasi' 
    ]);
    Route::post('/donasi-search', [
        'as' => 'donasi-search', 'uses' => 'HomeController@donasi' 
    ]);
    Route::get('/detail/{id}', [
        'as' => 'detail', 'uses' => 'HomeController@detail' 
    ]);
    Route::get('/detailberita/{id}', [
        'as' => 'detailberita', 'uses' => 'HomeController@detailberita' 
    ]);
    Route::get('/detailacara/{id}', [
        'as' => 'detailacara', 'uses' => 'HomeController@detailacara' 
    ]);
    Route::get('/proses/{id}', [
        'as' => 'proses', 'uses' => 'HomeController@proses' 
    ]);
    Route::post('/proses', [
        'as' => 'proses-donasi', 'uses' => 'HomeController@store'
    ]);
    Route::get('/payment/{id}', [
        'as' => 'payment', 'uses' => 'HomeController@payment'
    ]);
    Route::post('/payment', [
        'as' => 'updatepic', 'uses' => 'HomeController@fileupload'
    ]);
    Route::get('/berita', [
        'as' => 'berita', 'uses' => 'HomeController@berita' 
    ]);
    Route::get('/acara', [
        'as' => 'acara', 'uses' => 'HomeController@acara' 
    ]);
    Route::post('/acara-search', [
        'as' => 'acara-search', 'uses' => 'HomeController@acara' 
    ]);
    Route::get('/about', [
        'as' => 'about', 'uses' => 'HomeController@about' 
    ]);
    Route::get('/profile', [
        'as' => 'profile', 'uses' => 'HomeController@profile' 
    ]);
    Route::get('/forgot_password', [
        'as' => 'forgot_password', 'uses' => 'HomeController@forgot_password' 
    ]);
    Route::get('/mytransaction', [
        'as' => 'mytransaction', 'uses' => 'HomeController@mytransaction' 
    ]);
    Route::get('/register_user', [
        'as' => 'register_user', 'uses' => 'HomeController@register' 
    ]);
    Route::post('/create_user', [
        'as' => 'create_user', 'uses' => 'SocialAuth@create_user'
    ]);
    Route::post('/update_profile', [
        'as' => 'update_profile', 'uses' => 'HomeController@update_profile'
    ]);
    Route::post('/update_password', [
        'as' => 'update_password', 'uses' => 'HomeController@update_password'
    ]);
    Route::post('/proses_password', [
        'as' => 'proses_password', 'uses' => 'HomeController@proses_password'
    ]);
    Route::post('/donasi_yukamal', [
        'as' => 'donasi_yukamal', 'uses' => 'HomeController@donasi_yukamal'
    ]);
    Route::get('/cancel/{id}', [
        'as' => 'cancel', 'uses' => 'HomeController@cancel'
    ]);
    Route::get('auth/{provider}', 'SocialAuth@redirectToProvider');
    Route::get('auth/{provider}/callback', 'SocialAuth@handleProviderCallback');
    Route::get('/detail/{id}/export-excel-finance/', [
        'as' => 'export-excel-finance', 'uses' => 'HomeController@export_excel_finance'
    ]);
    Route::get('/detail/{id}/export-pdf-finance/', [
        'as' => 'export-pdf-finance', 'uses' => 'HomeController@export_pdf_finance'
    ]);
});

Route::get('/get_subcategory/{id}', ['as' => 'get-subcategory', 'uses' => 'SubCategory@getSubCategory']);
Route::get('/city/{id}', ['as' => 'get-city', 'uses' => 'Area@getCity']);
Route::get('/districts/{id}', ['as' => 'get-districts', 'uses' => 'Area@getDisctricts']);
Route::get('/alldistricts', ['as' => 'get-all-districts', 'uses' => 'Area@getAllDisctricts']);
Route::get('/villages/{id}', ['as' => 'get-villages', 'uses' => 'Area@getVillages']);
Route::get('/allvillages', ['as' => 'get-all-villages', 'uses' => 'Area@getAllVillages']);

Auth::routes();

// Route::get('/', 'HomeController@index')->name('home');

// ini bua frontend
Route::get('/getmasjid', ['as' => 'get-masjid', 'uses' => 'Masjid@getMasjid']);
Route::get('/getmasjid/{id}', ['as' => 'get_masjid_by_id', 'uses' => 'Masjid@getMasjid']);

