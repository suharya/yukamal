<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title"> Dashboard Admin</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
	    @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        @if(session()->has('alert'))
            <div class="alert alert-danger">
                {{ session()->get('alert') }}
            </div>
        @endif
		<div class="note note-info">
		    <p> A black page template with a minimal dependency assets to use as a base for any custom page you create </p>
		</div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->