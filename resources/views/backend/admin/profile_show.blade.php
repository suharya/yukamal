<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Detail Profil Pengguna</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        @if(session()->has('alert'))
            <div class="alert alert-danger">
                {{ session()->get('alert') }}
            </div>
        @endif
        <div class="portlet-body form">
            <a href="{{route('profilpengguna.edit', $user_id)}}"><button type="button" class="btn blue">Update</button></a>
            <a href="{{route('profilpengguna.password', $user_id)}}"><button type="button" class="btn yellow">Change Password</button></a>
            <div class="form-body">
                <div class="form-group">
                    <label class="control-label">Foto</label><br>
                    <div class="fileinput {{is_null($data->pic) ? 'fileinput-new' : 'fileinput-exist'}}" data-provides="fileinput">
                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                            @if(!is_null($data->pic))
                            <img src="{{url('uploads/'.$data->pic)}}">
                            @endif
                        </div>
                    </div>
                </div>
                <input type="hidden" class="form-control" id="image_old" value="{{$data->pic}}" name="image_old" maxlength="255">
                <input type="hidden" class="form-control" id="image_name" value="{{$data->pic}}" name="image_name" maxlength="255">
                <div class="form-group form-md-line-input form-md-floating-label">
                    <input type="text" class="form-control" id="form_control_1" value="{{$data->first_name}}" name="first_name" maxlength="255" disabled>
                    <label for="form_control_1">Nama Depan</label>
                    <span class="help-block">Maksimal 255 Karakter</span>
                </div>
                <div class="form-group form-md-line-input form-md-floating-label">
                    <input type="text" class="form-control" id="form_control_1" value="{{$data->last_name}}" name="last_name" maxlength="255" disabled>
                    <label for="form_control_1">Nama Belakang</label>
                    <span class="help-block">Maksimal 255 Karakter</span>
                </div>
                <div class="form-group form-md-line-input form-md-floating-label">
                    <select class="form-control edited" name="mosque_id" disabled>
                        <option value="0" {{$data->gender == '0' ? 'selected' : ''}}>Laki-laki</option>
                        <option value="1" {{$data->gender == '1' ? 'selected' : ''}}>Perempuan</option>
                    </select>
                    <label for="gender">Jenis Kelamin</label>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Tanggal Lahir</label>
                            <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy">
                                <input type="text" class="form-control" value="{{date('d-m-Y', strtotime($data->dob))}}"  name="dob" disabled>
                                <span class="input-group-btn">
                                    <button class="btn default" type="button" disabled>
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group form-md-line-input form-md-floating-label">
                    <input type="text" class="form-control" id="form_control_1" value="{{$data->email}}" name="email" maxlength="255" disabled>
                    <label for="form_control_1">Email</label>
                    <span class="help-block">Maksimal 255 Karakter</span>
                </div>
            </div>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->
<script>
  function remove(){
    document.getElementById("image_old").value = "";
  }
</script>