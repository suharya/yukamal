<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Edit Pembangunan</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        <div class="portlet-body form">
            <div class="form-body">
                <a href="{{route('pembangunan_masjid.edit', $data->id)}}"><button type="button" class="btn blue">Update</button></a>
                <a data-target="#static-{{$data->id}}" id="modal_delete" data-id="{{$data->id}}" data-toggle="modal"><button type="button" class="btn red">Delete</button></a>
                <div id="static-{{$data->id}}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                    <form method="post" action="{{route('pembangunan_masjid.destroy', $data->id)}}">
                    {{ method_field('DELETE') }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
                    <div class="modal-body">
                        <p> Apakah Anda yakin ini menghapus? </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
                        <button type="submit"  class="btn red">Delete</button>
                    </div>
                    </form>
                </div>
                <br><br>
                <table class="table">
                    <tbody>
                        <tr>
                            <th width="150">Masjid</th>
                            <td width="30">:</td>
                            <td>{{$masjid->name}}</td>
                        </tr>
                        <tr>
                            <th>Pembangunan ID</th>
                            <td>:</td>
                            <td>{{$dev->name}}</td>
                        </tr>
                        <tr>
                            <th>Bobot</th>
                            <td>:</td>
                            <td>{{$data->weight}}%</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="form-actions noborder">
                <a href="{{route('pembangunan_masjid.index')}}"><button type="button" class="btn default">Cancel</button></a>
            </div>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->