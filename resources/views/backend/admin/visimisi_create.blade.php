<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Add Visi & Misi</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        <div class="portlet-body form">
            <form role="form" method="post" action="{{ route('visimisi.store') }}">
                {{ csrf_field() }}
                <div class="form-body">
                    @if($role == 'admin')
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" name="mosque_identity">
                            @foreach($masjid as $m)
                            <option value="{{$m->id}}">{{$m->name}}</option>
                            @endforeach
                        </select>
                        <label for="status">Masjid</label>
                    </div>
                    @else
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" disabled>
                            @foreach($masjid as $m)
                            <option value="{{$m->id}}" {{$m->id == $MosqueDkmUser->mosque_id ? 'selected' : ''}}>{{$m->name}}</option>
                            @endforeach
                        </select>
                        <label for="status">Masjid</label>
                    </div>
                    <input type="hidden" class="form-control" id="mosque_identity" value="{{$MosqueDkmUser->mosque_id}}" name="mosque_identity" maxlength="255">
                    @endif
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <label for="form_control_1">Deskripsi</label>
                        <textarea class="form-control" name="description" id="description" rows="10" cols="50"></textarea>
                    </div>
                </div>
                <div class="form-actions noborder">
                    <button type="submit" class="btn blue">Submit</button>
                    <a href="{{route('visimisi.index')}}"><button type="button" class="btn default">Cancel</button></a>
                </div>
            </form>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->
<script src="{{url('/')}}/ckeditor/ckeditor.js" type="text/javascript"></script>
<script>
  var textarea = document.getElementById("description");
    CKEDITOR.replace(textarea,{
    language:'en-gb'
  });
  CKEDITOR.config.allowedContent = true;
</script>