<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Add Team</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        <div class="portlet-body form">
            <form role="form" method="post" enctype="multipart/form-data" action="{{ route('team.store') }}">
                {{ csrf_field() }}
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label">Foto</label><br>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                            <div>
                                <span class="btn red btn-outline btn-file">
                                    <span class="fileinput-new"> Select image </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" name="image"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" class="form-control" id="form_control_1" value="" name="fullname" maxlength="255" required>
                        <label for="form_control_1">Nama Lengkap</label>
                        <span class="help-block">Maksimal 255 Karakter</span>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" class="form-control" id="form_control_1" value="" name="position" maxlength="255" required>
                        <label for="form_control_1">Jabatan</label>
                        <span class="help-block">Maksimal 255 Karakter</span>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <label for="form_control_1">Biografi</label>
                        <textarea class="form-control" name="biography" id="biography" rows="3" required></textarea>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <label for="form_control_1">Email</label>
                        <input type="email" class="form-control" name="email" id="email" rows="3" required>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <label for="form_control_1">Facebook</label>
                        <input type="text" class="form-control" name="facebook" id="facebook" rows="3" required>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <label for="form_control_1">Twitter</label>
                        <input type="text" class="form-control" name="twitter" id="twitter" rows="3" required>
                    </div>
                </div>
                <div class="form-actions noborder">
                    <button type="submit" class="btn blue">Submit</button>
                    <a href="{{route('team.index')}}"><button type="button" class="btn default">Cancel</button></a>
                </div>
            </form>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->