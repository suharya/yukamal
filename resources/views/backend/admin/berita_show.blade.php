<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Detail Berita</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        <div class="portlet-body form">
            <div class="form-body">
                <a href="{{route('berita.edit', $data->id)}}"><button type="button" class="btn blue">Update</button></a>
                <a data-target="#static-{{$data->id}}" id="modal_delete" data-id="{{$data->id}}" data-toggle="modal"><button type="button" class="btn red">Delete</button></a>
                <div id="static-{{$data->id}}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                    <form method="post" action="{{route('berita.destroy', $data->id)}}">
                    {{ method_field('DELETE') }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
                    <div class="modal-body">
                        <p> Apakah Anda yakin ini menghapus? </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
                        <button type="submit"  class="btn red">Delete</button>
                    </div>
                    </form>
                </div>
                <br><br>
                <table class="table">
                    <tbody>
                        <tr>
                            <th width="150">Tanggal</th>
                            <td width="30">:</td>
                            <td>{{date('d-m-Y', strtotime($data->date))}}</td>
                        </tr>
                        <tr>
                            <th>Judul</th>
                            <td>:</td>
                            <td>{{$data->title}}</td>
                        </tr>
                        <tr>
                            <th>Deskripsi</th>
                            <td>:</td>
                            <td><?= $data->description;?></td>
                        </tr>
                        <tr>
                            <th>Foto</th>
                            <td>:</td>
                            <td><img style="width: 360px; height: 270px;" src="{{url('uploads/'.$data->pic)}}"></td>
                        </tr>
                    </tbody>
                </table>
                <a href="{{route('berita.index')}}"><button type="button" class="btn default">Back</button></a>
            </div>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->
<script src="{{url('/')}}/ckeditor/ckeditor.js" type="text/javascript"></script>
<script>
  var textarea = document.getElementById("description");
    CKEDITOR.replace(textarea,{
    language:'en-gb'
  });
  CKEDITOR.config.allowedContent = true;
</script>