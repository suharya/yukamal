<script src="{{url('/')}}/css/backend/jquery.dataTables.min.css" type="text/javascript"></script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title"> Kelurahan</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        @if(session()->has('alert'))
            <div class="alert alert-danger">
                {{ session()->get('alert') }}
            </div>
        @endif
		<div class="portlet-body">
            <div class="loading"><img style="max-width: 100px" src="{{ URL::asset('img/ring.gif') }}"> </img></div>
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="datalist">
                <thead>
                    <tr>
                        <td align="center" class="bold" width="40%"> Nama </td>
                        <td align="center" class="bold"> Kecamatan </td>
                        <td align="center" class="bold"> Actions </td>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td align="center" class="bold" width="40%"> Nama </td>
                        <td align="center" class="bold"> Kecamatan </td>
                        <td align="center" class="bold"> Actions </td>
                    </tr>
                </tfoot>
            </table>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->
<script src="{{url('/')}}/js/jquery.js"></script>
<script src="{{url('/')}}/js/jquery.dataTables.min.js"></script>
<script src="{{url('/')}}/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ajaxStart(function() {
        $(".loading").css("display","block");
    }).ajaxStop(function() {
        $(".loading").css("display","none");
    });
    $(document).ready(function() {
        $(function() {
            $('#datalist').DataTable({
                "processing": true,
                "serverSide": true,
                "autoWidth": true,
                "ajax" : {
                    "url" : "{{route('get-all-villages')}}",
                    "type":"GET",
                },
                "columns": [
                    { "data" : "name"},
                    { "data" : "district_name"},
                    { "data" : "actions","searchable":false,"orderable":false},
                ],initComplete: function () {
                    $(".loading").css("display","none");
                }
            });
        });
    });
</script>
<style>
    /* Loading spinner */
    /* Absolute Center Spinner */
    .loading {
        position: fixed;
        z-index: 999;
        height: 2em;
        width: 2em;
        overflow: show;
        margin: auto;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
    }
    /*!* Transparent Overlay *!*/
    .loading:before {
        content: '';
        display: block;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0,0,0,0.3);
    }
    /*!* :not(:required) hides these rules from IE9 and below *!*/
    .loading:not(:required) {
        /* hide "loading..." text */
        font: 0/0 a;
        color: transparent;
        text-shadow: none;
        background-color: transparent;
        border: 0;
    }
</style>