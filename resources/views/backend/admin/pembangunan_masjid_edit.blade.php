<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Edit Pembangunan</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        <div class="portlet-body form">
            <form role="form" method="post" action="{{ route('pembangunan_masjid.update', $data->id) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-body">
                    @if($role == 'admin')
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" name="mosque_id">
                            @foreach($masjid as $m)
                            <option value="{{$m->id}}" {{$m->id == $data->mosque_id ? 'selected' : ''}}>{{$m->name}}</option>
                            @endforeach
                        </select>
                        <label for="status">Masjid</label>
                    </div>
                    @else
                    <input type="hidden" class="form-control" id="mosque_id" value="{{$MosqueDkmUser->mosque_id}}" name="mosque_id" maxlength="255">
                    @endif
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" name="dev_id">
                            @foreach($dev as $d)
                            <option value="{{$d->id}}" {{$d->id == $data->dev_id ? 'selected' : ''}}>{{$d->name}}</option>
                            @endforeach
                        </select>
                        <label for="status">Pembangunan ID</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="number" step="any" class="form-control" id="form_control_1" value="{{$data->weight}}" name="weight" maxlength="45" required>
                        <label for="form_control_1">Bobot</label>
                        <span class="help-block">Satuan dalam Persen(%)</span>
                    </div>
                </div>
                <div class="form-actions noborder">
                    <button type="submit" class="btn blue">Submit</button>
                    <a href="{{route('pembangunan_masjid.index')}}"><button type="button" class="btn default">Cancel</button></a>
                </div>
            </form>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->