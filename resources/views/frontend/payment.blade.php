<style type="text/css">
	.table th, .table td {
		border: none;
	}
</style>
<section class="section-gap">
	<div class="container">
		<div class="col-md-12">
			<div class="text-center">
				<h1>Invoice Donasi</h1>
			</div>
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-8"><table class="table" style="margin-top: 1em">
					<tr>
						<td>No Transaksi</td>
						<td>: {{$donation->no_trans}}</td>
					</tr>
					<tr>
						<td>Nama</td>
						<td>: {{$donation->contributor_name}}</td>
					</tr>
					<tr>
						<td>Tipe Donasi</td>
						@if($donation->sub_category_id == 1)
						<td>: infaq</td>
						@elseif($donation->sub_category_id == 2)
						<td>: zakat</td>
						@else
						<td>: wakaf</td>
						@endif
					</tr>
					<tr>
						<td>Nominal</td>
						<td>: Rp {{number_format($donation->total)}}</td>
					</tr>
					<tr>
						<td>Bank Tujuan</td>
						<td>: {{$donation->bank}}</td>
					</tr>
					<tr>
						<td>Rekening Tujuan</td>
						<td>: 4445 0091 213</td>
					</tr>
					<tr>
						<td>Status</td>
						@if($donation->status == 0)
						<td>: Pending</td>
						@elseif($donation->status == 1)
						<td>: Dibatalkan</td>
						@elseif($donation->status == 3)
						<td>: Menunggu konfirmasi</td>
						@elseif($donation->status == 2)
						<td>: Sukses</td>
						@endif
					</tr>
					<form role="form" method="post" action="{{ route('updatepic') }}" enctype="multipart/form-data">
						{{ csrf_field() }}
						<input type="hidden" name="donation_id" value="{{$donation->id}}">
						<input type="hidden" name="donatur" value="{{$donation->contributor_name}}">
					@if($donation->status != 1)
					<tr>
						<td>Bukti Transfer</td>
						@if($donation->pic == NULL)
							<td>: <input type="file" name="image"></td>
						@else
							<td><img style="width: 50%;" src="{{ url('/uploads/'.$donation->pic) }}"></td>
						@endif
					</tr>
					@endif
					@if($donation->status == 0 && $donation->pic == NULL)
					<tr>
						<td></td>
						<td><button type="submit" class="primary-btn">Submit</button></td>
					</tr>
					@endif
					</form>
				</table></div>
			</div>
		</div>
	</div>
</section>