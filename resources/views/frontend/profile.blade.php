<style type="text/css">
	a.active {
		background-color: #44c19a !important;
		border-color: #44c19a !important;
	}
	a.active > a {
		color: white;
	}
</style>
<section class="section-gap">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="text-center" id="ava">
					@if($user->pic != null && $user->provider != null)
                    <img class="rounded-circle" style="width: 10em;" src="{{ $user->pic }}">
                	@elseif($user->pic != null && $user->provider == null)
                	<img class="rounded-circle" style="width: 10em;" src="{{ url('/uploads/'.$user->pic) }}">
                	@else
                	<img class="rounded-circle" style="width: 10em;" src="{{ url('/') }}/uploads/default.png">
                	@endif
				</div>
				<div class="text-center" style="margin-top: 1em;">
					<h3>{{ $users->name }}</h3>
				</div>
				<div class="text-center" style="padding: 1em 4em;">
					<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
						  <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">User Profile</a>
						  <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Change Password</a>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="tab-content" id="v-pills-tabContent">
					<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
						<div>
							<h3>PROFILE</h3>
						</div>
						<hr>
						<form class="form-horizontal" method="POST" action="{{ route('update_profile') }}">
	                        {{ csrf_field() }}
							<div class="form-group">
								<label>Username</label>
								<input class="form-control" type="text" name="username" value="{{$users->username}}" readonly>
								<input class="form-control" type="hidden" name="user_id" value="{{$users->id}}">
							</div>
							<div class="form-group">
								<label>First Name</label>
								<input class="form-control" type="text" name="first_name" value="{{$users->first_name}}">
							</div>
							<div class="form-group">
								<label>Last Name</label>
								<input class="form-control" type="text" name="last_name" value="{{$users->last_name}}">
							</div>
							<div class="form-group">
								<label>Date Of Birth</label>
								<input class="form-control" type="date" name="dob" value="{{$users->dob}}">
							</div>
							<div class="form-group">
								<label>Email</label>
								<input class="form-control" type="email" name="email" value="{{$users->email}}">
							</div>
							<div class="form-group">
								<label>Gender</label>
								<select class="form-control" name="gender">
									<option>-- Jenis Kelamin --</option>
									@if($users->gender == 0)
										<option value="0" selected>Laki - Laki</option>
										<option value="1">Perempuan</option>
									@elseif($users->gender == 1)
										<option value="0">Laki - Laki</option>
										<option value="1" selected>Perempuan</option>
									@else
										<option value="0">Laki - Laki</option>
										<option value="1">Perempuan</option>
									@endif
								</select>
							</div>
							<div class="mt-repeater">
								<label>Jamaah Masjid</label>
		                        <div data-repeater-list="jamaah">
		                            @if(!isset($jamaah[0]))
		                            <div data-repeater-item class="row">
		                                <div class="col-md-10">
		                                    <div class="form-group">
												<select class="form-control" name="mosque_id">
													<option>--Pilih Masjid--</option>
													@foreach($masjid as $m)
														<option value="{{$m->id}}">{{$m->name}}</option>
													@endforeach
												</select>
											</div>
		                                </div>
		                                <div class="col-md-2">
		                                    <div class="form-group">
		                                    	<label class="control-label"></label>
			                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger">
			                                        <i class="fa fa-close"></i>Delete
			                                    </a>
		                                    </div>
		                                </div>
		                            </div>
		                            @else
			                        	@foreach($jamaah as $value)
			                            <div data-repeater-item class="row">
			                                <div class="col-md-10">
			                                    <div class="form-group">
													<select class="form-control" name="mosque_id">
														<option>--Pilih Masjid--</option>
														@foreach($masjid as $m)
															@if($m->id == $value->mosque_id)
															<option value="{{$m->id}}" selected>{{$m->name}}</option>
															@else
															<option value="{{$m->id}}">{{$m->name}}</option>
															@endif
														@endforeach
													</select>
												</div>
			                                </div>
			                                <div class="col-md-2">
			                                    <div class="form-group">
			                                    	<label class="control-label"></label>
				                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger">
				                                        <i class="fa fa-close"></i>Delete
				                                    </a>
			                                    </div>
			                                </div>
			                            </div>
			                            @endforeach
		                            @endif
		                        </div>
		                        <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
		                            <i class="fa fa-plus"></i> Tambah Masjid
		                        </a>
		                    </div>
	                        <hr>
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Save</button>
							</div>
						</form>
					</div>
					<div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
						<div>
							<h3>PROFILE</h3>
						</div>
						<hr>
						<form class="form-horizontal" method="POST" action="{{ route('update_password') }}">
	                        {{ csrf_field() }}
	                        @if($users->provider == null)
							<div class="form-group">
								<label>Old Password</label>
								<input class="form-control" type="hidden" name="id" value="{{ $users->id }}">
								<input class="form-control" type="password" name="password" required>
							</div>
							@endif
							<div class="form-group">
								<label>New Password</label>
								<input class="form-control" type="hidden" name="id" value="{{ $users->id }}">
								<input class="form-control" type="password" name="password" required>
							</div>
							<div class="form-group">
								<label>Confirm New Password</label>
								<input class="form-control" type="password" name="confirm_password" required>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Update Password</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>			
	</div>
</section>