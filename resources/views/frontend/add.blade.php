<section class="section-gap" style="padding-bottom: 2em">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="container">
					<h1 style="padding-bottom: 1em;">Donasi Untuk {{$masjid->name}}</h1>
						<form role="form" method="post" action="{{ route('proses-donasi') }}">
						{{ csrf_field() }}
							<input class="form-control" type="hidden" name="mosque_identity" value="{{$masjid->id}}">
							<input class="form-control" type="hidden" name="user_id" value="{{$users->id}}">
							<input class="form-control" type="hidden" name="contributor_name" value="{{$users->name}}">
							<div class="form-group">
								<select class="form-control" name="sub_category_id" required>
									<option>- Pilih Donasi -</option>
									<option selected value="1">Infaq</option>
									<option value="2">Zakat</option>
									<option value="3">Wakaf</option>
								</select>
							</div>
							<div class="form-group">
								<input class="form-control" data-type="currency" type="text" name="nominal" placeholder="Nominal" required>
							</div>
              <hr>
							<div class="form-group">
								<!-- <label>Donasi Untuk YUKAMAL</label> -->
								<input class="form-control" data-type="currency" type="text" name="donasi_yukamal" placeholder="Donasi Untuk YUKAMAL">
							</div>
              <hr>
							<div class="form-group">
								<select class="form-control" name="bank" required>
									<option>- Pilih Bank -</option>
									<option selected value="BANK BCA">BANK BCA</option>
									<option value="BANK BNI">BANK BNI</option>
									<option value="BANK BNI SYARIAH">BANK BNI SYARIAH</option>
									<option value="BANK MANDIRI SYARIAH">BANK MANDIRI SYARIAH</option>
									<option value="BANK BRI">BANK BRI</option>
								</select>
							</div>
							<div class="form-group">
								<input type="checkbox" style="transform: scale(1.5);margin: 0 .5em;" name="anon" value="Hamba Allah">
								<label>Sebagai Hamba Allah</label>
							</div>
							<div>
								<button type="submit" class="primary-btn text-uppercase text-center btn-block">Yuk Donasi</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
<script type="text/javascript">
$("input[data-type='currency']").on({
    keyup: function() {
      formatCurrency($(this));
    },
    blur: function() { 
      formatCurrency($(this), "blur");
    }
});


function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val =left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val =input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}
</script>