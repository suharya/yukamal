<style type="text/css">
	.slick-prev:before, .slick-next:before {
		font-size: 40px !important;
	}
</style>
<section class="banner-area">
	<div class="container">
		<div class="row fullscreen align-items-center justify-content-between">
		<div style="position: absolute;top: 0;right: 0;left: 0;bottom: 0;background-color: #ababab8f;height: 550px;"></div>
			<div class="col-md-12 banner-left" style="text-align: center;">
				<h3>#YUKDONASI</h3>
				<h1>DONASI PEMBANGUNAN MASJID</h1>
				<a href="{{ route('donasi') }}" class="primary-btn text-uppercase">Donasi Sekarang</a>
			</div>
		</div>
	</div>					
</section>
		<!-- End banner Area -->
		<!-- Start fact Area -->
<section class="facts-area section-gap" id="facts-area" style="padding: 2em 0em 1em 0em">
	<div class="container">				
		<div class="row">
			<div class="col single-fact">
				<p style="font-size: 1.5em;font-weight: bold"><span class="counter">{{$musholla->total}}</span></p>
				<p>Musholla</p>
			</div>
			<div class="col single-fact">
				<p style="font-size: 1.5em;font-weight: bold"><span class="counter">{{$mesjid->total}}</span></p>
				<p>Masjid</p>
			</div>
			<div class="col single-fact">
				<p style="font-size: 1.5em;font-weight: bold">Rp <span class="counter">{{number_format($donasi_all->total)}}</span></p>
				<p>Tersalurkan</p>
			</div>
			<div class="col single-fact">
				<p style="font-size: 1.5em;font-weight: bold"><span class="counter">{{$jamaah->total}}</span></p>
				<p>Jama'ah</p>
			</div>	
			<div class="col single-fact">
				<p style="font-size: 1.5em;font-weight: bold"><span class="counter">{{$donatur->total}}</span></p>
				<p>Donatur</p>
			</div>												
		</div>
	</div>	
</section>
<!-- end fact Area -->	
<!-- start slider area -->
<section class="top-category-widget-area">
	<div class="container">
		<div class="row d-flex justify-content-center">
            <div class="menu-content pb-30 col-lg-8 mt-80">
                <div class="title text-center">
                    <h1 class="mb-10">#YUKGABUNG</h1>
                    <p>Gabung dengan acara yang diadakan oleh masjid</p>
                </div>
            </div>
        </div>
		<div class="slick" style="padding: 4em;">
			@foreach($acara as $acr)
			<div>
				<div class="slick-inner" style="padding: 1em;">
					<div class="single-cat-widget">
						<div class="content relative">
							<div class="overlay overlay-bg"></div>
						    <a href="{{ route('detailacara', $acr->id)}}" target="_blank">
						      <div class="thumb">
						  		 <img style="width: 100%;height: 200px;" class="content-image img-fluid d-block mx-auto" src="{{ url('/uploads/'.$acr->pic) }}" alt="">
						  		 <!-- <img url="{{ url('/uploads/'.$acr->pic) }}" alt=""> -->
						  	  </div>
						      <div class="content-details">
						        <h4 class="content-title mx-auto text-uppercase">{{$acr->name}}</h4>
						        <span></span>							        
						        <p>{{$acr->mosque_name}}</p>
						      </div>
						    </a>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>
<!-- end slider area -->
<!-- Start portfolio-area Area -->
<section class="portfolio-area section-gap" id="portfolio">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-30 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">#YUKDONASI</h1>
                    <p>Donasikan uang anda untuk pembangunan masjid</p>
                </div>
            </div>
        </div>
        
        <div class="filters-content" style="margin-top: 0px;">
            <div>
            	<div class="row">
	            	@foreach($masjid as $val)
	            	<div class="col-md-4">
	            		<div class="card" style="margin-top: 1em">
	                		<img style="width: 100%;height: 180px;" src="{{ url('/uploads/'.$val->pic) }}">
	                		<div class="card-title" style="padding: 1em 1em 0 1em;margin-bottom: -.5em">
	                			<h6>{{$val->name}}</h6>
	                			<p><?= str_limit($val->address, $limit = 30, $end = '...')?></p>
	                		</div>
	                		<hr style="margin:0;">
	                		<div class="card-body" style="padding: 1em 1em 0 1em;">
	                			<div class="row" style="font-size: .8em;text-align: center;">
	                    			<div class="col-md-6"><p>Donasi Terkumpul</p></div>
	                    			<div class="col-md-6"><p>Tahap Pembangunan</p></div>
	                			</div>
	                			<div class="row" style="text-align: center;">
	                    			<div class="col-md-6">
	                    				<p>Rp {{number_format($val->total) }}</p>
	                    			</div>
	                    			<div class="col-md-6">
	                    				<p>60 %</p>
	                    			</div>
	                			</div>
	                			<div class="row">
	                				<a href="{{ route('detail', $val->id)}}" class="primary-btn text-uppercase btn-block text-center">Donasi Sekarang</a>
	                			</div>
	                		</div>
	                	</div>
	            	</div>
	            	@endforeach
            	</div>
            </div>
        </div>
    </div>
</section>