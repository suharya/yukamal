<footer class="footer-area section-gap" style="padding: 40px 0 !important;">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h4>Yukamal</h4>
                    <p style="color: white;margin-top: 2em">
                        Perum Persada Depok BLOK c5/22<br>
                        Cimpaeun, Tapos - Depok<br>
                        Jawa Barat 16459
                    </p>
                    
                </div>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h4>Tentang Kami</h4>
                    <div style="margin-top: 2em">
                        <a style="color: white" href="{{ route('home') }}">Yukamal</a>
                    </div>
                    <div>
                        <a style="color: white" href="{{ route('about') }}">Visi &amp; Misi</a>
                    </div>
                    <div>
                        <a style="color: white" href="{{ route('about') }}">Tim Kami</a>
                    </div>
                    <div>
                        <a style="color: white" href="{{ route('donasi') }}">Donasi</a>
                    </div>
                    <div>
                        <a style="color: white" href="{{ route('berita') }}">Berita</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6 social-widget">
                <div class="single-footer-widget">
                    <h4>Follow Me</h4>
                    <p style="margin-top: 2em;color: white">Let's be social</p>
                    <div class="footer-social d-flex align-items-center">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-whatsapp"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
    <!-- End footer Area -->		
<script type="text/javascript">
	$('.slick').slick({
	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 1
	});

</script>
</body>
</html>