@include('layout.backend.header')
@include('layout.backend.sidebar')
@if(!empty($view))
	@include($view)
@endif
@include('layout.backend.footer')