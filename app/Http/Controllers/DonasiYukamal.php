<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\DonationYukamal;
use App\Image_uploaded;
use Image;
use File;
use Mail;
use App\Model\Users;

class DonasiYukamal extends Controller
{
    public $path;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        //DEFINISIKAN PATH
        $this->path = public_path('uploads');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        if ($role == 'admin') {
            $data['view'] = 'backend.admin.donasiyukamal';
            $data['title'] = 'Donasi YukAmal';
            $data['page'] = 'donasi-yukamal';
            $data['data'] = DonationYukamal::All();
            $data['role'] = $role;
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $data['role'] = Auth::user()->role;
        // if ($data['role'] == 'admin') {
        //     $data['view'] = 'backend.admin.donasiyukamal_create';
        //     $data['title'] = 'Add Donasi YukAmal';
        //     $data['page'] = 'donasi-yukamal';
        //     $data['category'] = DonationYukamal::all();
        //     return view('layout.backend.app', $data);
        // }else{
            return redirect('admin');
        // }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $donasiyukamal = new DonationYukamal;
            $donasiyukamal->mosque_identity = $request->mosque_identity;
            $donasiyukamal->description = $request->description;
            $donasiyukamal->save();

            return redirect(route('donasiyukamal.index'))->with('success', 'Tambah Donasi YukAmal Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.donasiyukamal_show';
            $data['title'] = 'Detail Donasi YukAmal';
            $data['page'] = 'donasi-yukamal';
            $data['data'] = DonationYukamal::where('id',$id)->first();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.donasiyukamal_edit';
            $data['title'] = 'Edit Donasi YukAmal';
            $data['page'] = 'donasi-yukamal';
            $data['data'] = DonationYukamal::where('id',$id)->first();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $donasiyukamal = DonationYukamal::where('id',$id)->first();
            $donasiyukamal->status = $request->status;

            if ($request->status == '2') {
                $to = Users::find($donasiyukamal->user_id)->email;
                Mail::send('backend.admin.email_donasi_yukamal', ['data' => ''], function ($message) use ($to) {
                    $message->from('support@yuk-amal.com', 'Support Yuk-Amal');
                    $message->to($to)->subject("Notifikasi Email Donasi Yuk-Amal");
                });
            }

            if (isset($request->image)) {
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpg,png,jpeg'
                ]);
                
                //JIKA FOLDERNYA BELUM ADA
                if (!File::isDirectory($this->path)) {
                    //MAKA FOLDER TERSEBUT AKAN DIBUAT
                    File::makeDirectory($this->path);
                }
                //MENGAMBIL FILE IMAGE DARI FORM
                $file = $request->file('image');
                //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
                $fileName = 'donasiyukamal_'.uniqid().'_'.date('ymdHis').'.' . $file->getClientOriginalExtension();
                //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
                Image::make($file)->save($this->path . '/' . $fileName);

                File::delete($this->path.'/'.$request->image_old);

                $donasiyukamal->pic = $fileName;
            }else{
                if (empty($request->image_old)) {
                    $donasiyukamal->pic = $request->image;

                    File::delete($this->path.'/'.$request->image_name);
                }
            }
            
            $donasiyukamal->update();

            return redirect(route('donasiyukamal.index'))->with('success', 'Update Donasi YukAmal Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $donasiyukamal = DonationYukamal::where('id', $id)->first();
        File::delete($this->path.'/'.$donasiyukamal->pic);
        $donasiyukamal->delete();
        return redirect(route('donasiyukamal.index'))->with('success', 'Delete Donasi YukAmal Berhasil!');
    }
}
