<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Regencies;
use App\Model\Provinces;

class Kota extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        if ($role == 'admin') {
            $data['view'] = 'backend.admin.kota';
            $data['title'] = 'Kota';
            $data['page'] = 'kota';
            $data['data'] = Regencies::All();
            $data['province'] = Provinces::All();
            $data['role'] = $role;
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.kota_create';
            $data['title'] = 'Add Kota';
            $data['page'] = 'kota';
            $data['user_id'] = Auth::User()->id;
            $data['province'] = Provinces::All();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $kota = new Regencies;
            $kota->name = strtoupper($request->name);
            $kota->province_id =$request->province_id;
            $kota->save();

            return redirect(route('kota.index'))->with('success', 'Tambah Kota Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.kota_show';
            $data['title'] = 'Detail Kota';
            $data['page'] = 'kota';
            $data['user_id'] = Auth::User()->id;
            $data['data'] = Regencies::where('id',$id)->first();
            $data['province'] = Provinces::All();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.kota_edit';
            $data['title'] = 'Edit Kota';
            $data['page'] = 'kota';
            $data['user_id'] = Auth::User()->id;
            $data['data'] = Regencies::where('id',$id)->first();
            $data['province'] = Provinces::All();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $kota = Regencies::where('id',$id)->first();
            $kota->name = strtoupper($request->name);
            $kota->province_id =$request->province_id;
            $kota->update();
            return redirect(route('kota.index'))->with('success', 'Update Kota Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kota = Regencies::where('id', $id);
        $kota->delete();
        return redirect(route('kota.index'))->with('success', 'Delete kota Berhasil!');
    }
}
