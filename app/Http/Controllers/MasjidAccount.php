<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\MosqueBank;
use App\Model\MosqueUser;
use App\Model\Bank;
use DB;

class MasjidAccount extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        if ($role == 'dkm') {
            $data['view'] = 'backend.admin.masjid_account';
            $data['title'] = 'Akun Bank Masjid';
            $data['page'] = 'masjid-account';
            $data['masjid'] = MosqueUser::select('mosque_id')->where('user_id',Auth::User()->id)->first();
            $data['data'] = DB::select("SELECT mb.*,b.name bank_name FROM mosque_bank mb join mosque m on m.id = mb.mosque_id join bank b on mb.bank_id = b.id where mb.mosque_id='".$data['masjid']->mosque_id."'");
            return view('layout.backend.app', $data);
        } else {
            return redirect('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'dkm') {
            $data['view'] = 'backend.admin.masjid_account_create';
            $data['title'] = 'Add Bank Masjid';
            $data['page'] = 'masjid-account';
            $data['user_id'] = Auth::User()->id;
            $data['bank'] = Bank::all();
            $data['masjid'] = MosqueUser::select('mosque_id')->where('user_id',Auth::User()->id)->first();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->all()['data'] as $val) {
            $dkm = new MosqueBank;
            $dkm->bank_id = $val['bank_id'];
            $dkm->no_account = $val['no_account'];
            $dkm->mosque_id = $request->mosque_id;
            $dkm->save();
        }

        return redirect(route('masjid_account.index'))->with('success', 'Daftar Akun Bank Masjid Berhasil!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'dkm') {
            $data['view'] = 'backend.admin.masjid_account_edit';
            $data['title'] = 'Edit Bank Masjid';
            $data['page'] = 'masjid-account';
            $data['user_id'] = Auth::User()->id;
            $data['bank'] = Bank::all();
            $data['masjid'] = MosqueUser::select('mosque_id')->where('user_id',Auth::User()->id)->first();
            $data['data'] = MosqueBank::where('mosque_id',$data['masjid']->mosque_id)->get();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dkm = MosqueBank::where('mosque_id', $id);
        $dkm->delete();
        foreach ($request->all()['data'] as $val) {
            $dkm = new MosqueBank;
            $dkm->bank_id = $val['bank_id'];
            $dkm->no_account = $val['no_account'];
            $dkm->mosque_id = $request->mosque_id;
            $dkm->save();
        }

        return redirect(route('masjid_account.index'))->with('success', 'Update Akun Bank Masjid Berhasil!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
