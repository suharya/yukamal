<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\SubCategoryModel;
use App\Model\CategoryModel;

class SubCategory extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        if ($role == 'admin') {
            $data['view'] = 'backend.admin.subcategory';
            $data['title'] = 'Sub Category';
            $data['page'] = 'sub-category';
            $data['data'] = SubCategoryModel::getSubCategory();
            $data['role'] = $role;
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.subcategory_create';
            $data['title'] = 'Add Sub Category';
            $data['page'] = 'sub-category';
            $data['category'] = CategoryModel::all();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $subcat = new SubCategoryModel;
            $subcat->category_id = $request->category_id;
            $subcat->nama = $request->nama;
            $subcat->save();

            return redirect(route('subcategory.index'))->with('success', 'Tambah Sub Kategori Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.subcategory_show';
            $data['title'] = 'Detail subcategory';
            $data['page'] = 'sub-category';
            $data['data'] = SubCategoryModel::where('id',$id)->first();
            $data['category'] = CategoryModel::where('id',$data['data']['category_id'])->first();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.subcategory_edit';
            $data['title'] = 'Edit subcategory';
            $data['page'] = 'subcategory';
            $data['data'] = SubCategoryModel::where('id',$id)->first();
            $data['category'] = CategoryModel::all();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $subcat = SubCategoryModel::where('id',$id)->first();
            $subcat->nama = $request->nama;
            $subcat->update();

            return redirect(route('subcategory.index'))->with('success', 'Update Sub Kategori Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subcat = SubCategoryModel::where('id', $id);
        $subcat->delete();
        return redirect(route('subcategory.index'))->with('success', 'Delete Sub Kategori Berhasil!');
    }

    public function getSubCategory($id){
        $subcat = SubCategoryModel::where('category_id', $id)->get();
        return json_encode($subcat);
    }
}
