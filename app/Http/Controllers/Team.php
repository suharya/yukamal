<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\TeamModel;
use App\Image_uploaded;
use Image;
use File;

class Team extends Controller
{
    public $path;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        //DEFINISIKAN PATH
        $this->path = public_path('uploads');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        if ($role == 'admin') {
            $data['view'] = 'backend.admin.team';
            $data['title'] = 'Team';
            $data['page'] = 'team';
            $data['data'] = TeamModel::All();
            $data['role'] = $role;
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.team_create';
            $data['title'] = 'Add Team';
            $data['page'] = 'team';
            $data['category'] = TeamModel::all();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'fullname'  => 'required|max:255',
            'position' => 'required|max:255',
            'biography' => 'required',
        ]);

        try {
            $team = new TeamModel;
            $team->fullname = $request->fullname;
            $team->position = $request->position;
            $team->biography = $request->biography;
            $team->twitter = $request->twitter;
            $team->facebook = $request->facebook;
            $team->email = $request->email;

            if (isset($request->image)) {
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpg,png,jpeg'
                ]);
                
                //JIKA FOLDERNYA BELUM ADA
                if (!File::isDirectory($this->path)) {
                    //MAKA FOLDER TERSEBUT AKAN DIBUAT
                    File::makeDirectory($this->path);
                }
                
                //MENGAMBIL FILE IMAGE DARI FORM
                $file = $request->file('image');
                //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
                $fileName = 'team_'.uniqid().'_'.date('ymdHis').'.' . $file->getClientOriginalExtension();
                //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
                Image::make($file)->save($this->path . '/' . $fileName);

                $team->pic = $fileName;
            }
            
            $team->save();

            return redirect(route('team.index'))->with('success', 'Tambah Team Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.team_show';
            $data['title'] = 'Detail Team';
            $data['page'] = 'team';
            $data['user_id'] = Auth::User()->id;
            $data['data'] = TeamModel::where('id',$id)->first();
            $data['category'] = TeamModel::all();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.team_edit';
            $data['title'] = 'Edit Team';
            $data['page'] = 'team';
            $data['user_id'] = Auth::User()->id;
            $data['data'] = TeamModel::where('id',$id)->first();
            $data['category'] = TeamModel::all();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'fullname'  => 'required|max:255',
            'position' => 'required|max:255',
            'biography' => 'required',
        ]);
        
        try {
            $team = TeamModel::where('id',$id)->first();
            $team->fullname = $request->fullname;
            $team->position = $request->position;
            $team->biography = $request->biography;
            $team->twitter = $request->twitter;
            $team->facebook = $request->facebook;
            $team->email = $request->email;

            if (isset($request->image)) {
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpg,png,jpeg'
                ]);
                
                //JIKA FOLDERNYA BELUM ADA
                if (!File::isDirectory($this->path)) {
                    //MAKA FOLDER TERSEBUT AKAN DIBUAT
                    File::makeDirectory($this->path);
                }
                //MENGAMBIL FILE IMAGE DARI FORM
                $file = $request->file('image');
                //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
                $fileName = 'team_'.uniqid().'_'.date('ymdHis').'.' . $file->getClientOriginalExtension();
                //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
                Image::make($file)->save($this->path . '/' . $fileName);

                File::delete($this->path.'/'.$request->image_old);

                $team->pic = $fileName;
            }else{
                if (empty($request->image_old)) {
                    $team->pic = $request->image;

                    File::delete($this->path.'/'.$request->image_name);
                }
            }

            $team->update();

            return redirect(route('team.index'))->with('success', 'Update Team Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team = TeamModel::where('id', $id)->first();
        File::delete($this->path.'/'.$team->pic);
        $team->delete();
        return redirect(route('team.index'))->with('success', 'Delete Team Berhasil!');
    }
}
