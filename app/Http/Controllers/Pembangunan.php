<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\PembangunanModel;

class Pembangunan extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        if ($role == 'admin') {
            $data['view'] = 'backend.admin.pembangunan';
            $data['title'] = 'Bobot Pembangunan';
            $data['page'] = 'pembangunan';
            $data['data'] = PembangunanModel::All();
            $data['role'] = $role;
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.pembangunan_create';
            $data['title'] = 'Add Bobot Pembangunan';
            $data['page'] = 'pembangunan';
            $data['user_id'] = Auth::User()->id;
            $data['category'] = PembangunanModel::all();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $cat = new PembangunanModel;
            $cat->name = $request->name;
            $cat->weight = $request->weight;
            $cat->save();

            return redirect(route('pembangunan.index'))->with('success', 'Tambah Bobot Pembangunan Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.pembangunan_show';
            $data['title'] = 'Detail pembangunan';
            $data['page'] = 'pembangunan';
            $data['user_id'] = Auth::User()->id;
            $data['data'] = PembangunanModel::where('id',$id)->first();
            $data['category'] = PembangunanModel::all();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.pembangunan_edit';
            $data['title'] = 'Edit pembangunan';
            $data['page'] = 'pembangunan';
            $data['user_id'] = Auth::User()->id;
            $data['data'] = PembangunanModel::where('id',$id)->first();
            $data['category'] = PembangunanModel::all();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $cat = PembangunanModel::where('id',$id)->first();
            $cat->name = $request->name;
            $cat->weight = $request->weight;
            $cat->update();

            return redirect(route('pembangunan.index'))->with('success', 'Update Bobot Pembangunan Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat = PembangunanModel::where('id', $id);
        $cat->delete();
        return redirect(route('pembangunan.index'))->with('success', 'Delete Bobot Pembangunan Berhasil!');
    }
}
