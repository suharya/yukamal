<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\NewsModel;
use App\Image_uploaded;
use Image;
use File;

class Berita extends Controller
{
    public $path;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        //DEFINISIKAN PATH
        $this->path = public_path('uploads');
    }

    public function index()
    {
        $role = Auth::user()->role;
        if ($role == 'admin') {
            $data['view'] = 'backend.admin.berita';
            $data['title'] = 'Berita';
            $data['page'] = 'berita';
            $data['data'] = NewsModel::All();
            $data['role'] = $role;
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.berita_create';
            $data['title'] = 'Add berita';
            $data['page'] = 'berita';
            $data['category'] = NewsModel::all();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (is_null($request->title) || is_null($request->description)) {
            return redirect(route('berita.create'))->with('alert', 'Title / Deskripsi tidak boleh kosong!');
        }

        try {
            $berita = new NewsModel;
            $berita->date = date('Y-m-d', strtotime($request->date));
            $berita->title = $request->title;
            $berita->description = $request->description;

            if (isset($request->image)) {
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpg,png,jpeg'
                ]);
                
                //JIKA FOLDERNYA BELUM ADA
                if (!File::isDirectory($this->path)) {
                    //MAKA FOLDER TERSEBUT AKAN DIBUAT
                    File::makeDirectory($this->path);
                }
                
                //MENGAMBIL FILE IMAGE DARI FORM
                $file = $request->file('image');
                //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
                $fileName = 'berita_'.uniqid().'_'.date('ymdHis').'.' . $file->getClientOriginalExtension();
                //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
                Image::make($file)->save($this->path . '/' . $fileName);

                $berita->pic = $fileName;
            }
            
            $berita->save();

            return redirect(route('berita.index'))->with('success', 'Tambah Berita Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.berita_show';
            $data['title'] = 'Detail berita';
            $data['page'] = 'berita';
            $data['user_id'] = Auth::User()->id;
            $data['data'] = NewsModel::where('id',$id)->first();
            $data['category'] = NewsModel::all();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.berita_edit';
            $data['title'] = 'Edit berita';
            $data['page'] = 'berita';
            $data['user_id'] = Auth::User()->id;
            $data['data'] = NewsModel::where('id',$id)->first();
            $data['category'] = NewsModel::all();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (is_null($request->title) || is_null($request->description)) {
            return redirect(route('berita.create'))->with('alert', 'Title / Deskripsi tidak boleh kosong!');
        }
        
        try {
            $berita = NewsModel::where('id',$id)->first();
            $berita->date = date('Y-m-d', strtotime($request->date));
            $berita->title = $request->title;
            $berita->description = $request->description;

            if (isset($request->image)) {
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpg,png,jpeg'
                ]);
                
                //JIKA FOLDERNYA BELUM ADA
                if (!File::isDirectory($this->path)) {
                    //MAKA FOLDER TERSEBUT AKAN DIBUAT
                    File::makeDirectory($this->path);
                }
                //MENGAMBIL FILE IMAGE DARI FORM
                $file = $request->file('image');
                //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
                $fileName = 'berita_'.uniqid().'_'.date('ymdHis').'.' . $file->getClientOriginalExtension();
                //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
                Image::make($file)->save($this->path . '/' . $fileName);

                File::delete($this->path.'/'.$request->image_old);

                $berita->pic = $fileName;
            }else{
                if (empty($request->image_old)) {
                    $berita->pic = $request->image;

                    File::delete($this->path.'/'.$request->image_name);
                }
            }

            $berita->update();
            return redirect(route('berita.index'))->with('success', 'Update Berita Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = NewsModel::where('id', $id)->first();
        File::delete($this->path.'/'.$berita->pic);
        $berita->delete();
        return redirect(route('berita.index'))->with('success', 'Delete Berita Berhasil!');
    }
}
