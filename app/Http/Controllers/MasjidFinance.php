<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\MosqueFinance;
use App\Model\MosqueFinanceDetail;
use App\Model\Mosque;
use App\Model\MosqueDkmUser;
use App\Model\CategoryModel;
use App\Model\SubCategoryModel;
use App\Model\Users;

class MasjidFinance extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        $data['view'] = 'backend.admin.keuangan';
        $data['title'] = 'Keuangan';
        $data['page'] = 'keuangan';
        $data['role'] = $role;
        if ($role == 'admin') {
            $data['data'] = MosqueFinance::getKeuangan();
        } else {
            $MosqueDkmUser = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
            $data['data'] = MosqueFinance::getKeuanganDkm($MosqueDkmUser->mosque_id);
        }

        return view('layout.backend.app', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.keuangan_create';
        $data['title'] = 'Add Keuangan';
        $data['page'] = 'keuangan';
        $data['category'] = CategoryModel::all();
        $data['sub_category'] = SubCategoryModel::where('category_id',1)->get();
        $data['user_id'] = Auth::user()->id;
        $data['mosque'] = Mosque::all();
        $data['MosqueDkmUser'] = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
        return view('layout.backend.app', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $keuangan = new MosqueFinance;
            $keuangan->date = date('Y-m-d', strtotime($request->date));
            $keuangan->user_id = $request->user_id;
            $keuangan->mosque_id = $request->mosque_id;
            $keuangan->save();

            foreach ($request->finance as $rf) {
                $detail = new MosqueFinanceDetail;
                $detail->mosque_finance_id = $keuangan->id;
                $detail->category_id = $rf['category_id'];
                $detail->sub_category_id = $rf['sub_category_id'];
                $detail->information = $rf['information'];
                $detail->nominal = $rf['nominal'];
                $detail->save();
            }

            return redirect(route('keuangan.index'))->with('success', 'Tambah Keuangan Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.keuangan_show';
        $data['title'] = 'Detail Keuangan';
        $data['page'] = 'keuangan';
        $data['detail'] = MosqueFinanceDetail::where('id',$id)->first();
        $data['data'] = MosqueFinance::where('id',$data['detail']['mosque_finance_id'])->first();
        $data['category'] = CategoryModel::where('id',$data['detail']['category_id'])->first();
        $data['sub_category'] = SubCategoryModel::where('id',$data['detail']['sub_category_id'])->first();
        $data['mosque'] = Mosque::find($data['data']['mosque_id']);
        $data['user'] = Users::find($data['data']->user_id);
        return view('layout.backend.app', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.keuangan_edit';
        $data['title'] = 'Edit Keuangan';
        $data['page'] = 'keuangan';
        $data['user_id'] = Auth::User()->id;
        $data['detail'] = MosqueFinanceDetail::where('id',$id)->first();
        $data['data'] = MosqueFinance::where('id',$data['detail']['mosque_finance_id'])->first();
        $data['category'] = CategoryModel::all();
        $data['sub_category'] = SubCategoryModel::where('category_id',$data['detail']->category_id)->get();
        $data['mosque'] = Mosque::all();
        $data['MosqueDkmUser'] = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
        return view('layout.backend.app', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            foreach ($request->finance as $rf) {
                $a = str_replace('.', '', substr($rf['nominal'], 2,-3)).str_replace(',', '.', substr($rf['nominal'], -3));
                $detail = MosqueFinanceDetail::find($id);
                $detail->category_id = $rf['category_id'];
                $detail->sub_category_id = $rf['sub_category_id'];
                $detail->information = $rf['information'];
                $detail->nominal = floatval($a);
                $detail->update();
            }

            $keuangan = MosqueFinance::where('id',$detail->mosque_finance_id)->first();
            $keuangan->date = date('Y-m-d', strtotime($request->date));
            $keuangan->user_id = $request->user_id;
            $keuangan->mosque_id = $request->mosque_id;
            $keuangan->update();

            return redirect(route('keuangan.index'))->with('success', 'Update Keuangan Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $financeDetail = MosqueFinanceDetail::find($id);

        if(!is_null($financeDetail)){
            $financeId = $financeDetail->mosque_finance_id;
            $financeDetail->delete();
            $exist = MosqueFinanceDetail::where('mosque_finance_id',$financeId)->get();
            if (count($exist) == 0) {
                $finance = MosqueFinance::find($financeId);
                $finance->delete();
            }
        }

        return redirect(route('keuangan.index'))->with('success', 'Delete Keuangan Berhasil!');
    }
}
