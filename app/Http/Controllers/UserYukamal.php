<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Users;
use App\Model\UserProfile;

class UserYukamal extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        if ($role == 'admin') {
            $data['view'] = 'backend.admin.user';
            $data['title'] = 'User Yukamal';
            $data['page'] = 'useryukamal';
            $data['data'] = Users::All();
            $data['role'] = $role;
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.user_create';
            $data['title'] = 'Add User';
            $data['page'] = 'user';
            $data['user_id'] = Auth::User()->id;
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $exist = Users::where('username',$request->username)->exists();
        $emailExist = Users::where('email',$request->email)->exists();

        if ($exist) {
            return redirect(route('useryukamal.create'))->with('alert', 'Username sudah terpakai!');
        }
        if ($emailExist) {
            return redirect(route('useryukamal.create'))->with('alert', 'Email sudah terpakai!');
        }
        if ($request->password !== $request->password_confirm) {
            return redirect(route('useryukamal.create'))->with('alert', 'Password Confirmation tidak sesuai!');
        }
        
        try {
            $user = new Users;
            $user->name = $request->name;
            $user->username = $request->username;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->role = $request->role;
            $user->save();
            $id = $user->id;

            $profile = new UserProfile;
            $profile->user_id = $id;
            $profile->first_name = $request->username;
            $profile->save();

            return redirect(route('useryukamal.index'))->with('success', 'Daftar user Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('useryukamal.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.user_edit';
            $data['title'] = 'Edit User';
            $data['page'] = 'useryukamal';
            $data['data'] = Users::where('id',$id)->first();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = Users::where('id',$id)->first();
            $user->name = $request->name;
            $user->username = $request->username;
            $user->email = $request->email;
            if (!empty($request->password)) {
                if ($request->password !== $request->password_confirm) {
                    return redirect(route('useryukamal.edit',$id))->with('alert', 'Password Confirmation tidak sesuai!');
                }
                $user->password = bcrypt($request->password);
            }
            $user->role = $request->role;
            $user->update();

            return redirect(route('useryukamal.index'))->with('success', 'Update user Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dkm = Users::where('id', $id);
        $dkm->delete();
        return redirect(route('useryukamal.index'))->with('success', 'Delete user Berhasil!');
    }
}
