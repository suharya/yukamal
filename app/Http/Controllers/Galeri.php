<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\MosqueGaleri;
use App\Model\Mosque;
use App\Model\MosqueDkmUser;
use App\Image_uploaded;
use Image;
use File;

class Galeri extends Controller
{
    public $path;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        //DEFINISIKAN PATH
        $this->path = public_path('uploads');
    }

    public function index()
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.galeri';
        $data['title'] = 'Galeri';
        $data['page'] = 'galeri';
        if ($data['role'] == 'admin') {
            $data['data'] = MosqueGaleri::getGaleri();
        } else {
            $data['MosqueDkmUser'] = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
            $data['data'] = MosqueGaleri::getGaleriDkm($data['MosqueDkmUser']->mosque_id);
        }
        
        return view('layout.backend.app', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.galeri_create';
        $data['title'] = 'Add Galeri';
        $data['page'] = 'galeri';
        $data['masjid'] = Mosque::all();
        $data['MosqueDkmUser'] = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
        return view('layout.backend.app', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $galeri = new MosqueGaleri;
            $galeri->description = $request->description;
            $galeri->mosque_id = $request->mosque_id;

            if (!is_null($request->image)) {
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpg,png,jpeg'
                ]);
                
                //JIKA FOLDERNYA BELUM ADA
                if (!File::isDirectory($this->path)) {
                    //MAKA FOLDER TERSEBUT AKAN DIBUAT
                    File::makeDirectory($this->path);
                }
                
                //MENGAMBIL FILE IMAGE DARI FORM
                $file = $request->file('image');
                //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
                $fileName = 'galeri_'.uniqid().'_'.date('ymdHis').'.' . $file->getClientOriginalExtension();
                //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
                Image::make($file)->save($this->path . '/' . $fileName);

                $galeri->pic = $fileName;
            }

            $galeri->save();

            return redirect('admin/galeri')->with('success', 'Daftar Galeri Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.galeri_show';
        $data['title'] = 'Detail galeri Masjid/Musholla';
        $data['page'] = 'galeri';
        $data['data'] = MosqueGaleri::find($id);
        $data['masjid'] = Mosque::All();
        return view('layout.backend.app', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.galeri_edit';
        $data['title'] = 'Edit galeri Masjid/Musholla';
        $data['page'] = 'galeri';
        $data['data'] = MosqueGaleri::find($id);
        $data['masjid'] = Mosque::All();
        $data['MosqueDkmUser'] = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
        return view('layout.backend.app', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $galeri = MosqueGaleri::where('id',$id)->first();
            $galeri->mosque_id = $request->mosque_id;
            $galeri->description = $request->description;

            if (isset($request->image)) {
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpg,png,jpeg'
                ]);

                //JIKA FOLDERNYA BELUM ADA
                if (!File::isDirectory($this->path)) {
                    //MAKA FOLDER TERSEBUT AKAN DIBUAT
                    File::makeDirectory($this->path);
                }
                //MENGAMBIL FILE IMAGE DARI FORM
                $file = $request->file('image');
                //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
                $fileName = 'galeri_'.uniqid().'_'.date('ymdHis').'.' . $file->getClientOriginalExtension();
                //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
                Image::make($file)->save($this->path . '/' . $fileName);

                File::delete($this->path.'/'.$request->image_old);

                $galeri->pic = $fileName;
            }else{
                if (empty($request->image_old)) {
                    $galeri->pic = $request->image;

                    File::delete($this->path.'/'.$request->image_name);
                }
            }

            $galeri->update();

            return redirect('admin/galeri')->with('success', 'Update Galeri Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $galeri = MosqueGaleri::find($id);
        File::delete($this->path.'/'.$galeri->pic);
        $galeri->delete();
        return redirect(route('galeri.index'))->with('success', 'Delete Galeri Berhasil!');
    }
}
