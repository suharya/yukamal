<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Admin extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        if ($role == 'admin') {
            $data['view'] = 'backend.admin.dashboard';
            $data['title'] = 'Dashboard';
            $data['page'] = 'dashboard';
            $data['role'] = $role;

            return view('layout.backend.app', $data);
        }elseif ($role == 'dkm') {
            return redirect()->route('profilpengguna.show',Auth::user()->id);
        }else{
            return redirect('/');
        }
    }
}