<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\SejarahModel;
use App\Model\Mosque;
use App\Model\MosqueDkmUser;
use App\Image_uploaded;
use Image;
use File;

class Sejarah extends Controller
{
    public $path;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        //DEFINISIKAN PATH
        $this->path = public_path('uploads');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        $data['view'] = 'backend.admin.sejarah';
        $data['title'] = 'Sejarah';
        $data['page'] = 'sejarah';
        $data['role'] = $role;
        if ($role == 'admin') {
            $data['data'] = SejarahModel::getHistory();
        }else{
            $MosqueDkmUser = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
            $sejarah = SejarahModel::where('mosque_identity',$MosqueDkmUser->mosque_id)->first();
            if (is_null($sejarah)) {
                return redirect()->route('sejarah.create');
            } else {
                return redirect()->route('sejarah.show',$sejarah->id);
            }
        }
        return view('layout.backend.app', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.sejarah_create';
        $data['title'] = 'Add Sejarah';
        $data['page'] = 'sejarah';
        $data['category'] = SejarahModel::all();
        $data['masjid'] = Mosque::all();
        $data['MosqueDkmUser'] = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
        return view('layout.backend.app', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $sejarah = new SejarahModel;
            $sejarah->mosque_identity = $request->mosque_identity;
            $sejarah->description = $request->description;

            if (isset($request->image)) {
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpg,png,jpeg'
                ]);
                
                //JIKA FOLDERNYA BELUM ADA
                if (!File::isDirectory($this->path)) {
                    //MAKA FOLDER TERSEBUT AKAN DIBUAT
                    File::makeDirectory($this->path);
                }
                
                //MENGAMBIL FILE IMAGE DARI FORM
                $file = $request->file('image');
                //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
                $fileName = 'sejarah_'.uniqid().'_'.date('ymdHis').'.' . $file->getClientOriginalExtension();
                //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
                Image::make($file)->save($this->path . '/' . $fileName);

                $sejarah->pic = $fileName;
            }
            
            $sejarah->save();

            return redirect(route('sejarah.index'))->with('success', 'Tambah Sejarah Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.sejarah_show';
        $data['title'] = 'Detail Sejarah';
        $data['page'] = 'sejarah';
        $data['user_id'] = Auth::User()->id;
        $data['data'] = SejarahModel::where('id',$id)->first();
        $data['category'] = SejarahModel::all();
        $data['masjid'] = Mosque::all();
        return view('layout.backend.app', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.sejarah_edit';
        $data['title'] = 'Edit Sejarah';
        $data['page'] = 'sejarah';
        $data['user_id'] = Auth::User()->id;
        $data['data'] = SejarahModel::where('id',$id)->first();
        $data['category'] = SejarahModel::all();
        $data['masjid'] = Mosque::all();
        return view('layout.backend.app', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $sejarah = SejarahModel::where('id',$id)->first();
            $sejarah->mosque_identity = $request->mosque_identity;
            $sejarah->description = $request->description;

            if (isset($request->image)) {
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpg,png,jpeg'
                ]);
                
                //JIKA FOLDERNYA BELUM ADA
                if (!File::isDirectory($this->path)) {
                    //MAKA FOLDER TERSEBUT AKAN DIBUAT
                    File::makeDirectory($this->path);
                }
                //MENGAMBIL FILE IMAGE DARI FORM
                $file = $request->file('image');
                //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
                $fileName = 'sejarah_'.uniqid().'_'.date('ymdHis').'.' . $file->getClientOriginalExtension();
                //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
                Image::make($file)->save($this->path . '/' . $fileName);

                File::delete($this->path.'/'.$request->image_old);

                $sejarah->pic = $fileName;
            }else{
                if (empty($request->image_old)) {
                    $sejarah->pic = $request->image;

                    File::delete($this->path.'/'.$request->image_name);
                }
            }

            $sejarah->update();

            return redirect(route('sejarah.index'))->with('success', 'Update sejarah Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sejarah = SejarahModel::where('id', $id)->first();
        File::delete($this->path.'/'.$sejarah->pic);
        $sejarah->delete();
        return redirect(route('sejarah.index'))->with('success', 'Delete sejarah Berhasil!');
    }
}
