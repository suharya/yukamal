<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Model\Mosque;
use App\Model\MosqueJamaah;
use App\Model\UserProfile;
use App\Model\MosqueDkmUser;
use App\User;

class Pengguna extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        if ($role == 'admin') {
            $data['view'] = 'backend.admin.pengguna';
            $data['title'] = 'Pengguna';
            $data['page'] = 'pengguna';
            $data['data'] = DB::table('users as u')->leftJoin('user_profile as up','u.id','=','up.user_id')->select('u.username','u.email','u.role','up.*','u.id as user_id')->get();
            $data['role'] = $role;
            return view('layout.backend.app', $data);
        }
        return redirect('admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect('admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return redirect('admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.pengguna_show';
            $data['title'] = 'Detail Pengguna';
            $data['page'] = 'pengguna';
            $data['user_id'] = Auth::User()->id;
            $data['data'] = DB::table('users as u')->leftJoin('user_profile as up','u.id','=','up.user_id')->leftJoin('mosque_ummat as mu','u.id','=','mu.user_id')->leftJoin('mosque_dkm_user as mdu','u.id','=','mdu.user_id')->leftJoin('mosque as m','m.id','=','mu.mosque_id')->select('u.username','u.email','u.role','up.*','u.id as user_id','m.id as mosque_id','m.name as mosque_name','mdu.mosque_id as mosque_dkm')->where('u.id',$id)->first();
            $data['mosque'] = Mosque::all();
            $mosque_ummat = MosqueJamaah::where('user_id',$id)->select('mosque_id');
            if ($mosque_ummat->exists()) {
                foreach ($mosque_ummat->get() as $mu) {
                    $data['mosque_ummat'][] = $mu->mosque_id;
                }
            } else {
                $data['mosque_ummat'] = null;
            }
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.pengguna_edit';
            $data['title'] = 'Edit Pengguna';
            $data['page'] = 'pengguna';
            $data['user_id'] = Auth::User()->id;
            $data['data'] = DB::table('users as u')->leftJoin('user_profile as up','u.id','=','up.user_id')->leftJoin('mosque_ummat as mu','u.id','=','mu.user_id')->leftJoin('mosque_dkm_user as mdu','u.id','=','mdu.user_id')->leftJoin('mosque as m','m.id','=','mu.mosque_id')->select('u.username','u.email','u.role','up.*','u.id as user_id','m.id as mosque_id','m.name as mosque_name','mdu.mosque_id as mosque_dkm')->where('u.id',$id)->first();
            $data['mosque'] = Mosque::all();
            $mosque_ummat = MosqueJamaah::where('user_id',$id)->select('mosque_id');
            if ($mosque_ummat->exists()) {
                foreach ($mosque_ummat->get() as $mu) {
                    $data['mosque_ummat'][] = $mu->mosque_id;
                }
            } else {
                $data['mosque_ummat'] = null;
            }
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->role == 'dkm') {
            $request->mosque_id_jamaah = '';
            if ($request->mosque_id_dkm == '') {
                return redirect(route('pengguna.index'))->with('alert', 'Harus pilih masjid untuk DKM!');
            }
        } elseif ($request->role == 'member') {
            $request->mosque_id_dkm = '';
            if ($request->mosque_id_jamaah == '') {
                return redirect(route('pengguna.index'))->with('alert', 'Pilihan Masjid tidak boleh kosong!');
            }
        }

        try {
            $up = UserProfile::where('user_id',$id)->first();

            if (is_null($up)) {
                $up = new UserProfile;
                $up->user_id = $id;
                $up->first_name = $request->first_name;
                $up->last_name = $request->last_name;
                $up->gender = $request->gender;
                $up->address = $request->address;
                $up->phone = $request->phone;
                $up->dob = date('Y-m-d', strtotime($request->date));
                $up->save();
            } else {
                $up->user_id = $id;
                $up->first_name = $request->first_name;
                $up->last_name = $request->last_name;
                $up->gender = $request->gender;
                $up->address = $request->address;
                $up->phone = $request->phone;
                $up->dob = date('Y-m-d', strtotime($request->date));
                $up->update();
            }

            $mdu = MosqueDkmUser::where('user_id',$id)->first();
            $mj = MosqueJamaah::where('user_id',$id);

            if ($request->role == 'member') {
                if (!$mj->exists()) {
                    foreach ($request->mosque_id_jamaah as $jamaah) {
                        $mj = new MosqueJamaah;
                        $mj->user_id = $id;
                        $mj->mosque_id = $jamaah;
                        $mj->save();
                    }
                }else{
                    $mj->delete();
                    foreach ($request->mosque_id_jamaah as $jamaah) {
                        $mj = new MosqueJamaah;
                        $mj->user_id = $id;
                        $mj->mosque_id = $jamaah;
                        $mj->save();
                    }
                }
                if(!is_null($mdu)) $mdu->delete();
            } else {
                if (is_null($mdu)) {
                    $mdu = new MosqueDkmUser;
                    $mdu->user_id = $id;
                    $mdu->mosque_id = $request->mosque_id_dkm;
                    $mdu->save();
                } else {
                    $mdu->user_id = $id;
                    $mdu->mosque_id = $request->mosque_id_dkm;
                    $mdu->update();
                }
                if(!is_null($mj)) $mj->delete();
            }

            $u = User::where('id',$id)->first();
            $u->email = $request->email;
            $u->role = $request->role;
            $u->update();

            return redirect(route('pengguna.index'))->with('success', 'Update Pengguna Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $u = User::where('id', $id);
        $u->delete();

        $up = UserProfile::where('user_id',$id)->first();
        $up->delete();
        
        $mj = MosqueJamaah::where('user_id',$id)->first();
        $mj->delete();

        return redirect(route('pengguna.index'))->with('success', 'Delete Pengguna Berhasil!');
    }
}
