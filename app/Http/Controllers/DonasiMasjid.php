<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Donation;
use App\Model\Mosque;
use App\Model\MosqueDkmUser;
use App\Model\MosqueFinance;
use App\Model\MosqueFinanceDetail;
use App\Model\SubCategoryModel;
use App\Model\Users;
use App\Image_uploaded;
use DB;
use Image;
use File;
use Mail;

class DonasiMasjid extends Controller
{
    public $path;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        //DEFINISIKAN PATH
        $this->path = public_path('uploads');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        $data['view'] = 'backend.admin.donasimasjid';
        $data['title'] = 'Donasi Masjid';
        $data['page'] = 'donasi-masjid';
        $data['role'] = $role;
        if ($role == 'admin') {
            $data['data'] = Donation::getDonations();
        } else {
            $data['MosqueDkmUser'] = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
            $data['data'] = Donation::where('mosque_identity',$data['MosqueDkmUser']->mosque_id)->where('status',2)->get();
        }
        return view('layout.backend.app', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $data['role'] = Auth::user()->role;
        // if ($data['role'] == 'admin') {
        //     $data['view'] = 'backend.admin.donasimasjid_create';
        //     $data['title'] = 'Add Donasi Masjid';
        //     $data['page'] = 'donasi-masjid';
        //     $data['category'] = Donation::all();
        //     return view('layout.backend.app', $data);
        // }else{
            return redirect('admin');
        // }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $donasimasjid = new Donation;
            $donasimasjid->mosque_identity = $request->mosque_identity;
            $donasimasjid->description = $request->description;
            $donasimasjid->save();

            return redirect(route('donasimasjid.index'))->with('success', 'Tambah Donasi Masjid Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.donasimasjid_show';
            $data['title'] = 'Detail Donasi Masjid';
            $data['page'] = 'donasi-masjid';
            $data['data'] = Donation::where('id',$id)->first();
            $data['mosque'] = Mosque::find($data['data']->mosque_identity);
            $data['category'] = SubCategoryModel::where('id',$data['data']->sub_category_id)->first();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.donasimasjid_edit';
            $data['title'] = 'Edit Donasi Masjid';
            $data['page'] = 'donasi-masjid';
            $data['data'] = Donation::where('id',$id)->first();
            $data['mosque'] = Mosque::find($data['data']->mosque_identity);
            $data['category'] = SubCategoryModel::where('id',$data['data']->sub_category_id)->first();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            if ($request->status == '2') {
                $donasimasjid = Donation::where('id',$id)->first();
                $to = Users::find($donasimasjid->user_id)->email;
                $masjid = Mosque::where('id',$donasimasjid->mosque_identity)->first();
                $subCat = SubCategoryModel::where('nama',$request->category_id)->first();

                $masjidFinance = MosqueFinance::where('date', DB::raw(date('Y-m-d', strtotime($donasimasjid->date))))->where('user_id',$donasimasjid->user_id)->where('mosque_id',$masjid->id)->exists();

                if (!$masjidFinance) {
                    $masjidFin = new MosqueFinance;
                    $masjidFin->date = date('Y-m-d', strtotime($donasimasjid->date));
                    $masjidFin->user_id = $donasimasjid->user_id;
                    $masjidFin->mosque_id = $masjid->id;
                    $masjidFin->save();
                    $masjidFinId = $masjidFin->id;

                    $masjidFinDet = new MosqueFinanceDetail;
                    $masjidFinDet->mosque_finance_id = $masjidFinId;
                    $masjidFinDet->category_id = $subCat->category_id;
                    $masjidFinDet->sub_category_id = $subCat->id;
                    $masjidFinDet->information = '';
                    $masjidFinDet->nominal = $donasimasjid->nominal;
                    $masjidFinDet->save();
                }

                Mail::send('backend.admin.email_donasi_masjid', ['data' => ''], function ($message) use ($to) {
                    $message->from('support@yuk-amal.com', 'Support Yuk-Amal');
                    $message->to($to)->subject("Notifikasi Email Donasi Masjid");
                });
            }

            $donasimasjid = Donation::where('id',$id)->first();
            $donasimasjid->status = $request->status;

            if (isset($request->image)) {
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpg,png,jpeg'
                ]);
                
                //JIKA FOLDERNYA BELUM ADA
                if (!File::isDirectory($this->path)) {
                    //MAKA FOLDER TERSEBUT AKAN DIBUAT
                    File::makeDirectory($this->path);
                }
                //MENGAMBIL FILE IMAGE DARI FORM
                $file = $request->file('image');
                //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
                $fileName = 'donasimasjid_'.uniqid().'_'.date('ymdHis').'.' . $file->getClientOriginalExtension();
                //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
                Image::make($file)->save($this->path . '/' . $fileName);

                File::delete($this->path.'/'.$request->image_old);

                $donasimasjid->pic = $fileName;
            }else{
                if (empty($request->image_old)) {
                    $donasimasjid->pic = $request->image;

                    File::delete($this->path.'/'.$request->image_name);
                }
            }

            $donasimasjid->update();

            return redirect(route('donasimasjid.index'))->with('success', 'Update Donasi Masjid Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $donasimasjid = Donation::where('id', $id)->first();
        File::delete($this->path.'/'.$donasimasjid->pic);
        $donasimasjid->delete();
        return redirect(route('donasimasjid.index'))->with('success', 'Delete Donasi Masjid Berhasil!');
    }
}
