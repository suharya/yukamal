<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MosqueGaleri extends Model
{
    protected $table = 'mosque_gallery';
    public $timestamps = false;

    static function getGaleri(){
    	$data = DB::table('mosque_gallery as mg')->join('mosque as m','m.id','mg.mosque_id')->select('mg.*','m.name as mosque_name')->get();
    	return $data;
    } 

    static function getGaleriDkm($id){
    	$data = DB::table('mosque_gallery as mg')
    	->join('mosque as m','m.id','mg.mosque_id')
    	->select('mg.*','m.name as mosque_name')
    	->where('m.id',$id)
    	->get();
    	return $data;
    } 
}
