<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MosqueEvent extends Model
{
    protected $table = 'mosque_event';
    public $timestamps = false;

    static function getEvent(){
    	$data = DB::table('mosque_event as me')->join('mosque as m','m.id','me.mosque_id')->select('me.*','m.name as mosque_identity','m.name as mosque_name')->get();
    	return $data;
    } 

    static function getEventDkm($id){
    	$data = DB::table('mosque_event as me')
    	->join('mosque as m','m.id','me.mosque_id')
    	->select('me.*','m.name as mosque_identity','m.name as mosque_name')
    	->where('m.id',$id)
    	->get();
    	return $data;
    }

    static function acara(){
        $data = DB::table('mosque_event as me')
        ->join('mosque as m','m.id','me.mosque_id')
        ->select('me.*','m.name as mosque')
        ->paginate(9);
        return $data;
    }

    static function acaraSearch($dats){
        $data = DB::table('mosque_event as me')
        ->join('mosque as m','m.id','me.mosque_id')
        ->select('me.*','m.name as mosque')
        ->where('me.name','like','%'.$dats['search'].'%')
        ->paginate(9);
        return $data;
    }
}
