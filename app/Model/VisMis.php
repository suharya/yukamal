<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class VisMis extends Model
{
    protected $table = 'visi_misi';
    public $timestamps = false;

    static function getVisMis(){
        $data = DB::table('mosque as m')
        ->join('visi_misi as v','m.id','v.mosque_identity')
        ->select('m.name as mosque_name','v.*')
        ->get();
        return $data;
    }

    static function getVisMisByMosqueId($id){
        $data = DB::table('mosque as m')
        ->join('visi_misi as v','m.id','v.mosque_identity')
        ->select('m.name as mosque_name','v.*')
        ->where('m.id',$id)
        ->first();
        return $data;
    }
}