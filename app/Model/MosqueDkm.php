<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MosqueDkm extends Model
{
    protected $table = 'mosque_dkm';
    public $timestamps = false;

    static function getDkm(){
    	$data = DB::table('mosque as m')
    	->join('mosque_dkm as md','m.id','md.mosque_identity')
    	->select('md.*','m.name as mosque_identity','m.identity')->get();
    	return $data;
    }

    static function getDkmPengurus($id){
    	$data = DB::table('mosque as m')
    	->join('mosque_dkm as md','m.id','md.mosque_identity')
    	->select('md.*','m.name as mosque_identity','m.identity')
    	->where('m.id',$id)
    	->get();
    	return $data;
    } 
}
