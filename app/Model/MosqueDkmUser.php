<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MosqueDkmUser extends Model
{
    protected $table = 'mosque_dkm_user';
    public $timestamps = false;
}
