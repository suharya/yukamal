<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class SejarahModel extends Model
{
    protected $table = 'history';
    public $timestamps = false;

    static function getHistory(){
        $data = DB::table('mosque as m')
        ->join('history as h','m.id','h.mosque_identity')
        ->select('m.name as mosque_name','h.*')
        ->get();
        return $data;
    }
}
