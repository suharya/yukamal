<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Model\Donation;

class Mosque extends Model
{
	protected $table = 'mosque';
    public $timestamps = false;

    static function getDetail($id){
        $now = date("Y-m-d H:i:s");
    	$data = DB::table('mosque as m')
    	->leftJoin('history as h','m.id','h.mosque_identity')
    	->leftJoin('visi_misi as vm','m.id','vm.mosque_identity')
    	->select('m.*','vm.description as visimisi','h.description','h.pic as image',DB::raw('DATEDIFF(m.estimate_date, "'.$now.'") as est_date'))
    	->where('m.id',$id)
    	->first();
    	return $data;
    }

    static function getAll(){
        $data = DB::table('mosque as m')
        ->leftJoin('donation as d','m.id','d.mosque_identity')
        ->select('m.*',DB::raw('SUM(d.nominal) as nominal') , 'd.status')
        ->groupBy('m.id')
        ->orderBy('m.id','desc')
        ->paginate(9);

        foreach ($data as $d) {
            $total = 0;
            $finance = Donation::where('mosque_identity',$d->id)->get();
            foreach ($finance as $f) {
                if ($f->status == '2') {
                    $total += floatval($f->nominal);
                }
            }
            $d->total = $total;
        }
        return $data;
    }

    static function getSearch($dats){
        $data = DB::table('mosque as m')
        ->leftJoin('donation as d','m.id','d.mosque_identity')
        ->select('m.*',DB::raw('SUM(d.nominal) as nominal') , 'd.status')
        ->where('m.name','like','%'.$dats['search'].'%')
        ->orWhere('m.kec_id',$dats['kec_id'])
        ->groupBy('m.id')
        ->orderBy('m.id','desc')
        ->paginate(9);

        foreach ($data as $d) {
            $total = 0;
            $finance = Donation::where('mosque_identity',$d->id)->get();
            foreach ($finance as $f) {
                if ($f->status == '2') {
                    $total += floatval($f->nominal);
                }
            }
            $d->total = $total;
        }
        return $data;
    }

    static function getGallery($id){
        $data = DB::table('mosque as m')
        ->leftJoin('mosque_gallery as mg','m.id','mg.mosque_id')
        ->select('m.name','mg.*')
        ->where('m.id',$id)
        ->get();
        return $data;
    }

    static function profit_mosque(){
        $data = DB::table('mosque as m')
        ->leftJoin('donation as d','m.identity','d.mosque_identity')
        ->select('m.*',DB::raw('SUM(d.nominal) as nominal'))
        ->where('m.identity','d.mosque_identity')
        ->groupBy('m.identity')
        ->get();
        return $data;
    }
}
