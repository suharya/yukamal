<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DonationYukamal extends Model
{
    protected $table = 'donation_yukamal';
    public $timestamps = false;
}
