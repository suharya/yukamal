<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MosqueFinance extends Model
{
    protected $table = 'mosque_finance';
    public $timestamps = false;

    static function getKeuangan(){
    	$data = DB::table('mosque_finance as mf')->join('mosque_finance_detail as mfc','mf.id','mfc.mosque_finance_id')->join('mosque as m','m.id','mf.mosque_id')->join('user_profile as up','mf.user_id','up.user_id')->join('category as c','c.id','mfc.category_id')->join('category_sub as sc','sc.id','mfc.sub_category_id')->select('mf.*','mfc.*','m.*','up.*','c.nama as category_name','sc.nama as sub_category_name','mfc.id as mosque_finance_detail_id')->get();
    	return $data;
    }

    static function getKeuanganDkm($id){
        $data = DB::table('mosque_finance as mf')
        ->join('mosque_finance_detail as mfc','mf.id','mfc.mosque_finance_id')
        ->join('mosque as m','m.id','mf.mosque_id')
        ->join('user_profile as up','mf.user_id','up.user_id')
        ->join('category as c','c.id','mfc.category_id')
        ->join('category_sub as sc','sc.id','mfc.sub_category_id')
        ->select('mf.*','mfc.*','m.*','up.*','c.nama as category_name','sc.nama as sub_category_name','mfc.id as mosque_finance_detail_id')
        ->where('mf.mosque_id',$id)
        ->get();
        return $data;
    }

    static function getKeuanganByMosqueId($id){
    	$data = DB::table('mosque_finance as mf')
            ->select(DB::raw('month(mf.date) as month'),
                'mf.date as date', 
                'c.nama as category', 
                'sc.nama as sub_category', 
                'mfd.information', 
                'mfd.nominal')
            ->join('mosque_finance_detail as mfd','mf.id','mfd.mosque_finance_id')
            ->join('category as c','c.id','mfd.category_id')
            ->join('category_sub as sc','sc.id','mfd.sub_category_id')
            ->where('mf.mosque_id',$id)
            ->where(DB::raw('year(mf.date)'),date('Y'))
            ->orderBy('month','asc')
            ->get();
    	return $data;
    } 
}
