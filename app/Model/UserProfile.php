<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserProfile extends Model
{
    protected $table = 'user_profile';
    public $timestamps = false;

    static function getProfile($id){
    	$data = DB::table('users as u')->leftJoin('user_profile as up','u.id','up.user_id')->select('u.email','up.provider','up.provider_id','up.*')->where('u.id',$id)->first();
    	return $data;
    }

    static function getProfile2($id){
    	$data = DB::table('users as u')
    	->join('user_profile as up','u.id','up.user_id')
    	->select('u.*','up.first_name','up.last_name','up.gender','up.provider','up.dob')
    	->where('u.id',$id)
    	->first();
    	return $data;
    }
}
