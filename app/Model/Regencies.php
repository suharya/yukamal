<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Regencies extends Model
{
    protected $table = 'regencies';
    public $timestamps = false;
}
