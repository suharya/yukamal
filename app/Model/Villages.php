<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Villages extends Model
{
    protected $table = 'villages';
    public $timestamps = false;

    public static function getVillages()
    {
        $disctricts = DB::table('villages as v')->leftJoin('districts as d','v.district_id','d.id')->select('v.*','d.name as district_name')->orderBy('v.name', 'asc')->get();
        return $disctricts;
    }
}
