<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MosqueBank extends Model
{
    protected $table = 'mosque_bank';
    public $timestamps = false;
}
