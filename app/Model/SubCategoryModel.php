<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class SubCategoryModel extends Model
{
    protected $table = 'category_sub';
    public $timestamps = false;

    public static function getSubCategory(){
    	return DB::table('category_sub as cs')->select('c.nama as category_name','cs.*')
    	->join('category as c','c.id','=','cs.category_id')->get();
    }
}
